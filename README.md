### anime_gan_v2 API

Prediction will return original photo with anime stylization

```shell
curl -X POST '<naogi_project_url>/predict' --form 'image=@"/path/to/file"' --output 'output_file'
```
