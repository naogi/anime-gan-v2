import torch

from PIL import Image
from naogi import NaogiModel, PilImageRenderer

class Model(NaogiModel):
    def load_model(self):
        device = 'cpu'

        self.model = torch.hub.load(
            "AK391/animegan2-pytorch:main",
            "generator",
            pretrained="face_paint_512_v1",
            device=device,
            progress=False,
        )

        self.face2paint = torch.hub.load(
            'AK391/animegan2-pytorch:main',
            'face2paint',
            size=512,
            device=device,
            side_by_side=False
        )

    def init_model(self):
        self.load_model()

    def prepare(self, params):
        self.image = Image.open(params['image']).convert('RGB')

    def render_options_dict(self):
        return {
            'content_format': 'PNG',
            'content_type': 'image/png'
        }

    def predict(self):
        return self.face2paint(self.model, self.image)

    def renderer(self):
        return PilImageRenderer
